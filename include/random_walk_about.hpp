#ifndef __random_walk_about_hpp
#define __random_walk_about_hpp

#include <vector>
#include <random>

namespace rwa {
  struct Actor {
    unsigned idx;
    float update;
  };

  class RWA {

  public:

    RWA();

    RWA(unsigned actors_size, unsigned domain_size);

    RWA(const RWA& other);

    RWA& operator=(const RWA& other);

    void init ();

    void traverse (unsigned nsteps);

    std::vector<Actor>& get_actors () { return actors; }
    void set_actors (const std::vector<Actor>& _actors) { actors = _actors; }

    std::vector<float>& get_domain () { return domain; }
    void set_domain (const std::vector<float>& _domain) { domain = _domain; }

    std::vector<float>& get_result () { return result; }

  protected:

    void single_step (Actor& actor);

    std::vector<Actor> actors;
    std::vector<float> domain;

    std::vector<float> result;

    unsigned seed;

  private:

    // std::default_random_engine generator;
    std::mt19937 generator;
    std::uniform_int_distribution<int> distribution_int;
    std::uniform_real_distribution<float> distribution_real;

  };
}


#endif
