#ifndef __random_walk_about_cuda_hpp
#define __random_walk_about_cuda_hpp

#include "curand_kernel.h"

#include "random_walk_about.hpp"

namespace rwa {

  class RWACUDA : public RWA {

  public:

    RWACUDA() : RWA() {}

    RWACUDA(unsigned actors_size, unsigned domain_size) : RWA(actors_size, domain_size) {}

    ~RWACUDA ();

    void init ();

    void traverse (unsigned nsteps);

    void traverse_atomic (unsigned nsteps);

    void traverse_sort_reduce (unsigned nsteps);

    void copy();

  private:

    Actor* actors_gpu;
    float* domain_gpu;
    float* result_gpu;

    bool alloced;

    curandState_t* states;

  };

}


#endif
