#ifndef __util_cuh
#define __util_cuhs

namespace util {
  __host__ __device__ double dot (double v1[3], double v2[3]);
}

#endif
