#ifndef __util_hpp
#define __util_hpp

#include <chrono>

namespace util {
  double invoke_dot (double v1[3], double v2[3]);

  inline std::chrono::time_point<std::chrono::high_resolution_clock> now () {
    return std::chrono::high_resolution_clock::now();
  }
}

#endif
