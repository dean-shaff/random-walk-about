#include <iostream>

#include "cuda_runtime.h"

#include "random_walk_about.hpp"
#include "random_walk_about_cuda.hpp"
#include "util.hpp" // already includes <chrono>


int main ()
{
  double vec1[3] = {1, 2, 3};
  double vec2[3] = {1, 2, 3};

  double result = util::invoke_dot(vec1, vec2);
  std::cerr << result << std::endl;

  unsigned actors_size = 200000;
  unsigned domain_size = 200000;
  rwa::RWA walker (actors_size, domain_size) ;
  walker.init();

  auto t0 = util::now();
  walker.traverse(100);
  std::chrono::duration<double, std::ratio<1>> delta_cpu = util::now() - t0;
  std::cerr << "Traverse CPU took " << delta_cpu.count() << " s" << std::endl;

  // std::vector<float> results = walker.get_result();
  // for (unsigned idx=0; idx<results.size(); idx++) {
  //   std::cerr << results[idx] << " ";
  // }
  // std::cerr << std::endl;

  rwa::RWACUDA walker_gpu (actors_size, domain_size);
  walker_gpu.init();
  cudaDeviceSynchronize();

  // t0 = util::now();
  // walker_gpu.traverse(100);
  // cudaDeviceSynchronize();
  // std::chrono::duration<double, std::ratio<1>> delta_gpu = util::now() - t0;
  // std::cerr << "Traverse GPU took " << delta_gpu.count() << " s" << std::endl;

  t0 = util::now();
  walker_gpu.traverse_atomic(100);
  cudaDeviceSynchronize();
  std::chrono::duration<double, std::ratio<1>> delta_gpu_atomic = util::now() - t0;
  std::cerr << "Traverse GPU (atomic) took " << delta_gpu_atomic.count() << " s" << std::endl;

  // t0 = util::now();
  // walker_gpu.traverse_sort_reduce(100);
  // cudaDeviceSynchronize();
  // std::chrono::duration<double, std::ratio<1>> delta_gpu_sort_reduce = util::now() - t0;
  // std::cerr << "Traverse GPU (sort reduce) took " << delta_gpu_sort_reduce.count() << " s" << std::endl;

  // results = walker_gpu.get_result();
  // for (unsigned idx=0; idx<results.size(); idx++) {
  //   std::cerr << results[idx] << " ";
  // }
  // std::cerr << std::endl;

  // std::vector<Actor> actors = walker_gpu.get_actors();
  // for (unsigned idx=0; idx<actors.size(); idx++) {
  //   std::cerr << actors[idx].update << " ";
  // }
  // std::cerr << std::endl;

}
