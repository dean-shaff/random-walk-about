#include "random_walk_about.hpp"

namespace rwa {

  RWA::RWA() {
    seed = 5489UL;
    generator = std::mt19937(seed);
    distribution_real = std::uniform_real_distribution<float> (0.0, 1.0);
  }

  RWA::RWA(unsigned actors_size, unsigned domain_size) {
    RWA();
    actors = std::vector<Actor>(actors_size);
    domain = std::vector<float>(domain_size);
    result = std::vector<float>(domain_size);
    distribution_int = std::uniform_int_distribution<int> (0, domain_size);
  }

  RWA::RWA(const RWA& other) {
    operator=(other);
  }

  RWA& RWA::operator= (const RWA& other) {
    if (this != &other) {
      actors = other.actors;
      domain = other.domain;

      result = other.result;

      generator = other.generator;
      distribution_int = other.distribution_int;
      distribution_real = other.distribution_real;
    }
    return *this;
  }

  void RWA::init () {
    for (unsigned iactor=0; iactor<actors.size(); iactor++) {
      actors[iactor].idx = iactor;
      actors[iactor].update = 0.0;
    }
  }

  void RWA::traverse (unsigned nsteps) {
    Actor actor;
    for (unsigned iactor=0; iactor<actors.size(); iactor++) {
      actor = actors[iactor];
      for (unsigned istep=0; istep<nsteps; istep++) {
        single_step(actor);
        result[actor.idx] += actor.update;
      }
    }
  }

  void RWA::single_step (Actor& actor) {
    // actor.idx = 0; //distribution_int(generator);
    actor.update = 1.0;
  }

}
