#include <iostream>
#include <stdio.h>

#include "cuda_runtime.h"
// #include "curand_kernel.h"
#include <thrust/sort.h>
#include <thrust/reduce.h>
#include <thrust/execution_policy.h>

#include "random_walk_about_cuda.hpp"

#define FULL_MASK 0xffffffff

__global__ void init_kernel (
  rwa::Actor* actors,
  unsigned actors_size
)
{
  const unsigned idx = blockDim.x*blockIdx.x + threadIdx.x;
  const unsigned total_size_x = gridDim.x*blockDim.x;

  if (idx > actors_size) {
    return;
  }

  for (unsigned iactor=idx; iactor<actors_size; iactor+=total_size_x) {
    actors[iactor].idx = iactor;
    actors[iactor].update = 1.0;
  }

}


__device__ void single_step_kernel (
  rwa::Actor* actor,
  unsigned domain_size,
  curandState_t* state
)
{
  float rand = curand_uniform(state);
  // (*actor).idx = 0.0; // (unsigned) (rand * domain_size);
  (*actor).update = 1.0;
}


__global__ void traverse_kernel_atomic (
  rwa::Actor* actors,
  float* result,
  const unsigned actors_size,
  const unsigned domain_size,
  const unsigned nsteps,
  curandState_t* states,
  const unsigned seed
)
{
  const unsigned idx = blockDim.x*blockIdx.x + threadIdx.x;
  const unsigned total_size_x = blockDim.x * gridDim.x;

  if (idx > actors_size) {
    return;
  }
  curand_init(seed, idx, 0, &states[idx]);
  curandState_t local_state = states[idx];
  rwa::Actor actor;
  for (unsigned iactor=idx; iactor<actors_size; iactor+=total_size_x) {
    actor = actors[iactor];
    for (unsigned istep=0; istep<nsteps; istep++) {
      single_step_kernel(&actor, domain_size, &local_state);
      atomicAdd(&result[actor.idx], actor.update);
    }
  }
}

__global__ void traverse_kernel_sort_reduce (
  rwa::Actor* actors,
  float* result,
  const unsigned actors_size,
  const unsigned domain_size,
  const unsigned nsteps,
  curandState_t* states,
  const unsigned seed
)
{

  const unsigned scratch_elements = blockDim.x;
  extern __shared__ unsigned char _scratch[];
  int* scratch_idx_sort = reinterpret_cast<int*>(_scratch);
  int* scratch_idx_reduce = reinterpret_cast<int*>(_scratch + sizeof(int)*scratch_elements);
  float* scratch_update = reinterpret_cast<float*>(_scratch + 2*sizeof(int)*scratch_elements);

  const unsigned idx = blockDim.x*blockIdx.x + threadIdx.x;
  const unsigned total_size_x = blockDim.x * gridDim.x;

  if (idx > actors_size) {
    return;
  }
  curand_init(seed, idx, 0, &states[idx]);
  curandState_t local_state = states[idx];
  rwa::Actor actor;
  for (unsigned iactor=idx; iactor<actors_size; iactor+=total_size_x) {
    actor = actors[iactor];
    for (unsigned istep=0; istep<nsteps; istep++) {
      scratch_idx_reduce[iactor] = -1;
      single_step_kernel(&actor, domain_size, &local_state);
      scratch_idx_sort[iactor] = actor.idx;
      scratch_update[iactor] = actor.update;
      __syncthreads();
      thrust::sort_by_key(thrust::seq,
        scratch_idx_sort, scratch_idx_sort + scratch_elements, scratch_update);
      thrust::reduce_by_key(thrust::seq,
        scratch_idx_sort, scratch_idx_sort + scratch_elements, scratch_update,
        scratch_idx_reduce, scratch_update);
      if (scratch_idx_reduce[iactor] != -1) {
        result[scratch_idx_reduce[iactor]] += scratch_update[iactor];
      }
      __syncthreads();
    }
  }
}


__device__ float warp_reduce_sum (float val) {
  for (unsigned offset = warpSize/2; offset > 0; offset >>= 1) {
    val += __shfl_down_sync(FULL_MASK, val, offset);
  }
  return val;
}

__global__ void traverse_kernel (
  rwa::Actor* actors,
  float* result,
  const unsigned actors_size,
  const unsigned domain_size,
  const unsigned nsteps,
  curandState_t* states,
  const unsigned seed
)
{
  const unsigned actor_idx = threadIdx.x;
  const unsigned total_size_actor = blockDim.x;

  if (actor_idx > actors_size) {
    return;
  }
  curand_init(seed, actor_idx, 0, &states[actor_idx]);
  curandState_t local_state = states[actor_idx];
  rwa::Actor actor;
  for (unsigned iactor=actor_idx; iactor<actors_size; iactor+=total_size_actor) {
    actor = actors[iactor];
    for (unsigned istep=0; istep<nsteps; istep++) {
      single_step_kernel(&actor, domain_size, &local_state);
      // result[actor.idx*actors_size + iactor] += actor.update;
      result[iactor*domain_size + actor.idx] += actor.update;
    }
  }
}

__global__ void sum_kernel(
  float* result,
  unsigned actors_size,
  unsigned domain_size
)
{
  extern __shared__ unsigned char _scratch[];
  float* scratch = reinterpret_cast<float*>(_scratch);

  const unsigned actor_idx = threadIdx.x;
  const unsigned total_size_actor = blockDim.x;

  const unsigned domain_idx = blockIdx.x;
  const unsigned total_size_domain = gridDim.x;

  const unsigned warp_idx = actor_idx % warpSize;
  const unsigned warp_num = actor_idx / warpSize;

  unsigned max_warp_num = total_size_actor / warpSize;
  if (max_warp_num == 0) {
    max_warp_num++;
  }
  float partial_sum = 0.0;
  for (unsigned iactor=actor_idx; iactor<actors_size; iactor+=total_size_actor) {
    // partial_sum += result[blockIdx.x * actors_size + actor_idx];
    partial_sum += result[iactor*domain_size + domain_idx];
  }
  // float partial_sum = result[actor_idx*domain_size + domain_idx];
  partial_sum = warp_reduce_sum(partial_sum);
  if (warp_idx == 0) {
    scratch[warp_num] = partial_sum;
  }
  __syncthreads();

  if (warp_num == 0) {
    if (warp_idx > max_warp_num) {
      partial_sum = 0;
    } else {
      partial_sum = scratch[warp_idx];
    }
    partial_sum = warp_reduce_sum(partial_sum);
    if (warp_idx == 0) {
      // result[domain_idx * actors_size] = partial_sum;
      result[domain_idx] = partial_sum;
    }
  }
}



namespace rwa {
  RWACUDA::~RWACUDA () {
    if (alloced) {
      cudaFree(actors_gpu);
      cudaFree(domain_gpu);
      cudaFree(result_gpu);
    }
  }
  void RWACUDA::copy () {
    unsigned actors_size = actors.size();
    unsigned domain_size = domain.size();

    cudaMemcpy(actors.data(), actors_gpu, actors_size*sizeof(Actor), cudaMemcpyDeviceToHost);
    cudaMemcpy(domain.data(), domain_gpu, domain_size*sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(result.data(), result_gpu, domain_size*sizeof(float), cudaMemcpyDeviceToHost);
  }

  void RWACUDA::init () {

    unsigned actors_size = actors.size();
    unsigned domain_size = domain.size();
    unsigned result_size = actors_size * domain_size;

    cudaMalloc((void**) &actors_gpu, actors_size*sizeof(Actor));
    cudaMalloc((void**) &domain_gpu, domain_size*sizeof(float));
    cudaMalloc((void**) &result_gpu, result_size*sizeof(float));
    cudaMemset(result_gpu, 0, result_size*sizeof(float));

    init_kernel<<<1, 1024>>>(actors_gpu, actors_size);

    cudaMalloc((void**)&states, actors_size*sizeof(curandState_t));

    alloced = true;
  }

  void RWACUDA::traverse_atomic (unsigned nsteps) {

    unsigned actors_size = actors.size();
    unsigned domain_size = domain.size();
    unsigned block_size = actors_size;
    unsigned grid_size = 1;
    if (block_size > 1024) {
      grid_size = block_size / 1024;
      block_size = 1024;
    }
    // std::cerr << "traverse_atomic: grid_size=" << grid_size
    //   << ", block_size=" << block_size << std::endl;
    traverse_kernel_atomic<<<grid_size, block_size>>>(
      actors_gpu, result_gpu, actors_size, domain_size, nsteps, states, seed
    );

  }

  void RWACUDA::traverse_sort_reduce (unsigned nsteps) {

    unsigned actors_size = actors.size();
    unsigned domain_size = domain.size();
    unsigned block_size = actors_size;
    unsigned grid_size = domain_size;

    size_t scratch_size = block_size * (sizeof(float) + 2*sizeof(int));
    traverse_kernel_sort_reduce<<<1, block_size, scratch_size>>>(
      actors_gpu, result_gpu, actors_size, domain_size, nsteps, states, seed
    );

  }
  void RWACUDA::traverse (unsigned nsteps) {

    unsigned actors_size = actors.size();
    unsigned domain_size = domain.size();
    unsigned result_size = actors_size * domain_size;
    unsigned block_size = actors_size;
    unsigned grid_size = domain_size;


    unsigned n_scratch_floats = (block_size / 32) + 1;
    traverse_kernel<<<1, block_size>>>(
      actors_gpu, result_gpu, actors_size, domain_size, nsteps, states, seed
    );
    sum_kernel<<<grid_size, block_size, sizeof(float)*n_scratch_floats>>>(
      result_gpu, actors_size, domain_size
    );

    // for (unsigned iactor=0; iactor<actors_size; iactor++) {
    //   for (unsigned idx=0; idx<domain_size; idx++) {
    //     std::cerr << result[iactor * domain_size + idx] << " ";
    //   }
    //   std::cerr << std::endl;
    // }
  }


}
